<?php
$recup = isset($_GET['sup']) ? $_GET['sup'] : "";
if (isset($_GET['sup']) && !empty($_GET['sup'])) {
    if ($recup == 'ok') {
        setcookie('nom', null, -1);
        setcookie('mdp', null, -1);
        header("Location: index.php");
    }
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        body {
            margin: 0;
            padding: 0;
        }

        #bandeau {
            width: 100%;
            height: 100px;
            background-color: blue;
            color: white;
            display: flex;
            justify-content: space-between;
            align-items: center;
        }

        h2 {
            margin-left: 20px;
        }

        a {
            margin-right: 20px;
            color: white;
            text-decoration: none;
        }

        #formulaire {
            width: 80%;
            margin: 10px auto;
        }

        form {
            display: flex;
            justify-content: space-between;
        }

        .champ {
            width: 25%;
        }

        input {
            padding: 6px 30px;
        }

        #valid {
            background-color: blue;
            color: white;
            border-radius: 8px;
        }

        #recap {
            width: 80%;
            display: flex;
            justify-content: space-around;
            margin: auto;
            border: 1px solid #DBDBDB;
            box-shadow: 0 0 2px 2px #DBDBDB;
            border-radius: 5px;
            text-align: center;
        }

        h3 {
            margin-left: 80px;
        }

        #general {
            width: 80%;
            margin: 10px auto 50px;
            border: 1px solid #DBDBDB;
            box-shadow: 0 0 2px 2px #DBDBDB;
            border-radius: 5px;
        }

        #block {
            display: flex;
            flex-wrap: wrap;
            align-content: flex-start;
            justify-content: space-evenly;
        }

        #block2 {
            padding: 12px;
            width: 13%;
            border: 1px solid rgba(102, 102, 102, 0.445);
            border-radius: 5px;
            margin: 3px 3px;
            text-align: center;
        }

        h5 {
            margin-left: 10px;
        }

        #bouton {
            display: flex;
            justify-content: space-evenly;
        }

        #vert {
            background-color: green;
            width: 13%;
            border: 1px solid #DBDBDB;
            border-radius: 5px;
            padding: 12px;
            font-size: 20px;
            text-align: center;
        }

        #rouge {
            background-color: red;
            width: 13%;
            border: 1px solid #DBDBDB;
            border-radius: 5px;
            padding: 12px;
            font-size: 20px;
            text-align: center;
        }

        .but {
            margin-right: 0;
        }
    </style>
</head>

<body>
    <?php
    //if(){
    $recupMontant = isset($_GET['montant']) ? intval($_GET['montant']) : 0;
    $recupDuree = isset($_GET['duree']) ? intval($_GET['duree']) : 0;
    $recupTaux = isset($_GET['taux']) ? intval($_GET['taux']) : 0;
    ?>
    <div id="bandeau">
        <h2>Accord de prêt</h2>
        <a href="?sup=ok">Déconnexion</a>
    </div>
    <div id="formulaire">
        <form method="GET">
            <input class="champ" type="text" name="montant" placeholder="Montant" value="<?php if (!empty($recupMontant)) {
                                                                                                echo $_GET['montant'];
                                                                                            } ?>">
            <input class="champ" type="text" name="duree" placeholder="Durée (années)" value="<?php if (!empty($recupDuree)) {
                                                                                                    echo $_GET['duree'];
                                                                                                } ?>">
            <input class="champ" type="text" name="taux" placeholder="Taux (%)" value="<?php if (!empty($recupTaux)) {
                                                                                            echo $_GET['taux'];
                                                                                        } ?>">
            <input id="valid" type="submit" value="Calculer">
        </form>
    </div>
    <?php
    $total = ($recupMontant * $recupTaux / 100) + $recupMontant;
    $calcul = $total / $recupDuree / 12;
    $mois = [
        'janvier',
        'fevrier',
        'mars',
        'avril',
        'mai',
        'juin',
        'juillet',
        'aout',
        'septembre',
        'octobre',
        'novembre',
        'decembre',
    ];
    if (!empty($_GET['montant']) && !empty($_GET['duree']) && !empty($_GET['taux'])) {
        echo "<h3>Récapitulatif</h3>
                    <div id='recap'>
                        <div>
                            <p>Montant</p>
                            <p>$recupMontant €</p>
                        </div>
                        <div>
                            <p>Durée</p>
                            <p>$recupDuree ans</p>
                        </div>
                        <div>
                            <p>Taux</p>
                            <p> $recupTaux %</p>
                        </div>
                        <div>
                            <p>Montant total dû</p>
                            <p>$total €</p>
                        </div>
                    </div>";
        echo '<h3>Echancier</h3>';
        echo "<div id='general'>";
        for ($y = 0; $y <= ($recupDuree - 1); $y++) {
            echo "<h5> Pour l'année 20" . ($y + 20) . "</h5>";
            echo '<div id="block">';
            for ($i = 0; $i <= 11; $i++) {
                echo "<div id=block2>$mois[$i] <br> $calcul €</div>";
            }
            echo '</div>';
        }
        echo '</div>';
        echo "<div id='bouton'>
                   <div id='vert'>
                        <a class='but' href='#'>Accepter</a>
                   </div>
                   <div id='rouge'>
                        <a class='but' href='?sup=ok'>Refuser</a>
                   </div>
                </div>";
    }
    ?>
</body>

</html>