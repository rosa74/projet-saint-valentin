
<?php
session_start();
$recupPseudo = isset($_GET['pseudo']) ? $_GET['pseudo'] : "";
$recupMdp = isset($_GET['mdp']) ? $_GET['mdp'] : "";
$recupConfMdp = isset($_GET['confmdp']) ? $_GET['confmdp'] : "";
$recupCheck = isset($_GET['check']) ? $_GET['check'] : "";
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        h2 {
            text-align: center;
        }

        #formulaire {
            border: 1px solid #DBDBDB;
            box-shadow: 0 0 2px 2px #DBDBDB;
            width: 30%;
            margin: 50px auto;
            border-radius: 5px;
        }

        form {
            text-align: center;
            padding: 10px
        }

        .champ {
            width: 60%;
            margin-bottom: 10px;
            padding: 6px 30px;
        }

        #valid {
            background-color: blue;
            color: white;
            padding: 5px 20px;
            border-radius: 8px;
            margin: 8px 0;
        }
    </style>
</head>

<body>
    <?php
    $x = null;
    var_dump($recupCheck);
    if ($recupPseudo == 'manu') {
        if ($recupMdp == 'enssop') {
            if ($recupConfMdp == $recupMdp) {
                if ($recupCheck == 'on') {
                    setcookie('nom', $recupPseudo, time() + 60 * 60, null, null, false, true);
                    setcookie('mdp', $recupMdp, time() + 60 * 60, null, null, false, true);
                    header("location: pret.php");
                } else {
                    header("location: pret.php");
                }
            } else {
                $x = "Les deux mots de passes ne sont pas identiques";
            }
        } else {
            $x = "Le mot de passe n'est pas correct";
        }
    } else {
        $x = "Le nom d'utilisateur n'est pas le bon";
    }
    ?>
    <div id="formulaire">
        <h2>Authentification</h2>
        <form method="GET">
            <input class="champ" type="text" name="pseudo" placeholder="Votre pseudo"><br>
            <input class="champ" type="text" name="mdp" placeholder="Votre mot de passe"><br>
            <input class="champ" type="text" name="confmdp" placeholder="Confirmer votre mot de passe"><br>
            <input type="checkbox" name="check">Se souvenir de moi pendant 30 minutes<br>
            <?php echo "<p>$x</p>"; ?>
            <input id="valid" type="submit" value="Se connecter">
        </form>
    </div>
    <?php
    ?>
</body>

</html>